-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2015 at 11:05 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `phpproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(300) NOT NULL,
  `Password` varchar(300) NOT NULL,
  `Email` varchar(300) NOT NULL,
  `Picture` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `Username`, `Password`, `Email`, `Picture`) VALUES
(8, 'Sofie', '$2a$08$FexwfEh5SW/jwiuWm3ryCupSutqDBtVjWDv5MHRjGeFjcP76beupq', 'sofie@hotmail.com', 0),
(9, 'Jonas', '$2a$08$jEmwg.IDDXrH0qWR2KFrj.zl1l0.qqxW3hjmrPJFhzVdeg2LCIRO.', 'jonas@hotmail.com', 0),
(10, 'Sander', '$2a$08$omFBbyR6YHfzdqhGaHywru0b66n2bmDvFnVcXGc.lw.dR7i8bS5Sm', 'sander@hotmail.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE IF NOT EXISTS `bookings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FirstNameGuide` varchar(500) NOT NULL,
  `LastNameGuide` varchar(500) NOT NULL,
  `idGuide` int(11) NOT NULL,
  `DateBooking` date NOT NULL,
  `nameUser` varchar(500) NOT NULL,
  `datum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUser` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=97 ;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `FirstNameGuide`, `LastNameGuide`, `idGuide`, `DateBooking`, `nameUser`, `datum`, `idUser`) VALUES
(95, 'Sander', 'Vankeer', 29, '2015-05-03', 'Sander Van Keer', '2015-05-18 19:33:31', '10206513042129176'),
(96, 'Jonas', 'Reymen', 30, '2015-06-29', 'Sander Van Keer', '2015-05-18 20:32:18', '10206513042129176');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(350) NOT NULL,
  `userid` varchar(350) NOT NULL,
  `guideid` varchar(350) NOT NULL,
  `message` varchar(350) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `name`, `userid`, `guideid`, `message`) VALUES
(18, 'Sander Van Keer', '10206513042129176', '29', 'Hallo Sander, alles goed?'),
(19, 'Sander Vankeer', '10206513042129176', '29', 'Hey Sander, jaja alles is in orde');

-- --------------------------------------------------------

--
-- Table structure for table `datums`
--

CREATE TABLE IF NOT EXISTS `datums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `datums`
--

INSERT INTO `datums` (`id`, `Date`) VALUES
(1, '2015-06-29'),
(2, '2015-05-03');

-- --------------------------------------------------------

--
-- Table structure for table `guide`
--

CREATE TABLE IF NOT EXISTS `guide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(300) NOT NULL,
  `LastName` varchar(300) NOT NULL,
  `Password` varchar(300) DEFAULT NULL,
  `Email` varchar(300) NOT NULL,
  `Age` varchar(10) NOT NULL,
  `Picture` varchar(300) NOT NULL,
  `Bio` varchar(300) NOT NULL,
  `Interest` varchar(300) NOT NULL,
  `IMDYear` int(11) NOT NULL,
  `TotalBookings` int(255) NOT NULL,
  `TotalRating` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `guide`
--

INSERT INTO `guide` (`id`, `FirstName`, `LastName`, `Password`, `Email`, `Age`, `Picture`, `Bio`, `Interest`, `IMDYear`, `TotalBookings`, `TotalRating`) VALUES
(29, 'Sander', 'Vankeer', '$2a$08$0qOoWTWZDzpb/SCgxU.PoO8R4NpnXWiW8oE5D4ag4BUjXJtAEc7Mu', 'sacevankeer@hotmail.com', '21', 'ik6.jpg', 'Hallo, ik ben Sander Van keer. 21 Jaar uit en zit in mijn tweede jaar IMD.', 'Development', 2, 1, 0),
(30, 'Jonas', 'Reymen', '$2a$08$ujUkqgneCb8Voqx6GzRO1ODwqiCZLPPp41KqyYYJJQVYYr2O6ANMm', 'jonasreymen@gmail.com', '19', 'jonas.jpg', 'HIk ben Jonas Reymen en ik kom uit Lummen. Ik wil je graag rondleiden!', 'Development', 2, 1, 0),
(31, 'Sofie', 'Keirsmaekers', '$2a$08$8zvfXnd574NirRcBrIda3O3I7aRk4jjQgxjhyzjx18VpOjFXCi55G', 'sofiekeirsmaekers@gmail.com', '19', 'sofie.jpg', 'Hallo Ik ben Sofie en ik heb voor deze richting gekozen omdat de mix van design en development mij wel aansprak.', 'Development', 2, 0, 0),
(32, 'Matthias', 'Christiaens', '$2a$08$L6p2X0Ux1VS6gvJxpHRLeeIs1/sqa.5JIrapVI5k64Ihp3.AkrBvy', 'matthiaschristiaens@gmail.com', '21', 'matthias.jpg', 'Ik ben Matthias!', 'Development', 2, 0, 0),
(33, 'Pieter', 'Tenret', '$2a$08$JngjOk/mM/pkfnHLxZ7qWO8lxI5yZD/jSt6A.n0xlFOaeTMeT..Da', 'pietertenret@gmail.com', '19', 'pieter.jpg', 'Ik ben Pieter Tenret.', 'Design', 2, 0, 0),
(35, 'Axel', 'Remue', '$2a$08$nrNgNlFqVjSTpsKvKgnCN.9.7bJGg2Riy6r9ynNfwkdKZy2GZnt8G', 'axelremue@gmail.com', 'Onbekend', 'default.png', 'Onbekend', 'Onbekend', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `quote`
--

CREATE TABLE IF NOT EXISTS `quote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Quote` varchar(500) NOT NULL,
  `feedback` varchar(300) NOT NULL,
  `GuideID` int(11) NOT NULL,
  `Rating` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `quote`
--

INSERT INTO `quote` (`id`, `Quote`, `feedback`, `GuideID`, `Rating`) VALUES
(1, 'dag goed verlopen!', '', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
