<?php
// start session
session_start();

// loading facebook srcfiles
require_once 'autoload.php';

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\GraphUser;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

// initialise app, helper and get session
$app_id = '480209908792754';
$app_secret = '86e572d72f8720363b0c01552953e064';
FacebookSession::setDefaultApplication($app_id, $app_secret);

$helper = new FacebookRedirectLoginHelper('http://localhost/rent-a-student/RAS/facebook/login_fb.php');
$session = $helper->getSessionFromRedirect();

// if session exists ,get user info
if(isset($session)) {
	$request = new FacebookRequest( $session, 'GET', '/me' );
	$response = $request->execute();
	$graph = $response->getGraphObject();

	$_SESSION['id']= $graph->getProperty('id');
	$_SESSION['name']= $graph->getProperty('name');
	$_SESSION['email']= $graph->getProperty('email');
	$_SESSION['status']= "user";
	header("Location: /rent-a-student/RAS/index.php/dashboard"); 
} else {
	$loginUrl = $helper->getLoginUrl();
	header("Location: ".$loginUrl);
}