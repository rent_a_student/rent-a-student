<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Test Login w/ Facebook</title>
</head>
<body>

  <div id="fb-root"></div>

  <!--facebook-->
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_NL/sdk.js#xfbml=1&version=v2.3&appId=216951336991";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
</script>

	<?php if(isset($_SESSION['id'])): ?>
		<h1><?php echo $_SESSION['name'] ?></h1>
		<img src="https://graph.facebook.com/<?php echo $_SESSION['id']; ?>/picture">
		<p><?php echo $_SESSION['email'] ?></p>
		
    <div class="fb-share-button" data-href="http://localhost/RAS/facebook/index.php" data-layout="button"></div>
    <a class="twitter-share-button"href="https://twitter.com/share">Tweet</a>
   

	<a href="/rent-a-student/RAS/index.php/guidelist"><button>Check out all guides!</button></a>

	<?php else: ?>
		<a href="login.php">Log in with Facebook</a>
	<?php endif ?>


</body>
</html>