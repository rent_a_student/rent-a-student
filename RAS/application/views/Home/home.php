
		<div class="cover">

		<div class="container">

		<div class="row">

			<div class="col-md-6">

				<div class="col-md-12 redbox box">

					<h1>Wil jij de school leren kennen?</h1>

					<h2>Schrijf je in en kies een gids</h2>

					<p>Ga je verder studeren en ben je net als ons helemaal zot van pixels en programmeren? In de professionele bachelor-opleiding Interactive Multimedia Design leer je de volledige mix tussen web design en development. Kom zeker eens een kijkje nemen!</p>

					<a class="btn btn-lg btn-callred" role="button" href="/rent-a-student/RAS/facebook/login_fb.php">Log in via facebook</a>

				</div>

			</div>

			<div class="col-md-6">

				<div class="col-md-12 bluebox box">

					<h1>Wil jij een gids worden?</h1>

					<h2>Schrijf je in en geef je eerste rondleiding</h2>

					<p>Ben je gepassioneerd door de opleiding? Schrijf je dan nu in en help ons! Wijs toekomstige studenten de weg naar onze opleiding, en wordt een echte IMD-hero. De richting en je docenten zullen je dankbaar zijn en wie weet zorg jij wel voor je eigen opvolging!</p>

					<a class="btn btn-lg btn-callblue" role="button" href="index.php/registreer">Registreer</a>
				</div>

			</div>

		</div>

	</div>

	</div>