<div class="container">
<div class="col-md-3"></div>
<div class="col-md-6">

<h1>Meld je aan als gids!</h1>

<?php if(isset($feedback)): ?>
			<h2><?php echo $feedback; ?></h2>
		<?php endif; ?>

		<form action="" method="post">

		<div class="form-group">
		<label for="email">Email</label>
		<input type="text" id="email" name="email" class="form-control">
		</div>

		<div class="form-group">
		<label for="password">Wachtwoord</label>
		<input type="password" id="password" name="password" class="form-control">
		</div>

		<button type="submit" name="btn_login" value="Login" type="button" class="btn btn-callred">Log in!</button>
	</form>

	<a href="/rent-a-student/RAS/index.php/registreer">Nieuw? Registreer hier!</a>

	</div>
	<div class="col-md-3"></div>
</div>
