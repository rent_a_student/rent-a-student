<div class="container">
<div class="col-md-3"></div>
<div class="col-md-6">

<h1>Meld je aan als admin!</h1>
    
    <form action="" method="post">
        
        <div class="form-group">
        <label for="username">Username</label>
        <input type="text" id="username" name="username" class="form-control">
		</div>

		<div class="form-group">
        <label for="password">Wachtwoord</label>
        <input type="password" id="password" name="password" class="form-control">
        </div>

        <button type="submit" id="btn" name="btn_login" value="Login" type="button" class="btn btn-callred">Log in!</button>
    
    </form>

</div>
<div class="col-md-3"></div>
</div>