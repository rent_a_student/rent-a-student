
<div class="container">
<div class="col-md-3"></div>
<div class="col-md-6">
	<?php echo validation_errors(); ?>
	<h1>Registreer als gids!</h1>

	<form action="" method="post">
		<div class="form-group">
		<label for="firstname">Voornaam</label>
		<input type="text" id="firstname" value="<?php echo set_value('firstname'); ?>" name="firstname" class="form-control">
		</div>
		
		<div class="form-group">
		<label for="lastname">Achternaam</label>
		<input type="text" id="lastname" value="<?php echo set_value('lastname'); ?>" name="lastname" class="form-control">
		</div>

		<div class="form-group">
		<label for="password">Wachtwoord</label>
		<input type="password" id="password" value="<?php echo set_value('password'); ?>" name="password" class="form-control">
		</div>

		<div class="form-group">
		<label for="email">Email</label>
		<input type="text" id="email" value="<?php echo set_value('email'); ?>" name="email" class="form-control" >
		</div>

		<div class="form-group">
		<label for="IMDYear">Jaar</label>
		<select name="IMDYear" id="IMDYear" class="form-control">
		  <option value="1" <?php echo set_select('IMDYear', '1'); ?>>1 IMD</option>
		  <option value="2" <?php echo set_select('IMDYear', '2'); ?>>2 IMD</option>
		  <option value="3" <?php echo set_select('IMDYear', '3'); ?>>3 IMD</option>
		</select>
		</div>
		

		<div class="form-group">
		<button type="submit" class="btn btn-callred">Registreer!</button>
		</div>
		
	</form>

	<a href="/rent-a-student/RAS/index.php/login">Heb je al een account? Log hier in!</a>
	</div>
	<div class="col-md-3"></div>
</div>

