<div class="container">
	<div class="col-md-3"></div>
		<div class="col-md-6">

			<h1>Chat</h1>
			<ul class="list-group">
				<?php foreach ($allmessages as $a): ?>
					<li class="list-group-item">
						<p><strong><?php echo $a['name'] . ": " ?></strong> <?php echo $a['message'] ?></p>
					</li>
				<?php endforeach ?>
			</ul>
			<br>
			<form action="" method="post">
				<textarea class="form-control" name="message" id="message" cols="30" rows="3"></textarea>
				<input type="submit" class="btn btn-default">
			</form>
		</div>
	<div class="col-md-3"></div>
</div>