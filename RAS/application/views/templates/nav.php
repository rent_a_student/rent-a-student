<?php if(!isset($_SESSION)){
	session_start();
} ?>
<nav class="navbar navbar-default">
	<div class="navbar-line"></div>
	<div class="container">
		<div class="container-fluid">

			<!--left-->

			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">

					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>

				</button>

				<?php 
				if(isset ($_SESSION['status'])) {

					$status = $_SESSION['status'];

					if($status === "admin"){
						echo '<a class="navbar-brand" href="/rent-a-student/RAS/index.php/admin/dashboard" role="button">Rent-a-Student</a>' ;
					} else if($status === "guide"){
						echo '<a class="navbar-brand" href="/rent-a-student/RAS/index.php/guide/boekingen" role="button">Rent-a-Student</a>';
					} else if($status === "user"){
						echo '<a class="navbar-brand" href="/rent-a-student/RAS/index.php/User/dashboard" role="button">Rent-a-Student</a>';
					}

				} else {

					echo '<a class="navbar-brand" href="/rent-a-student/RAS" role="button">Rent-a-Student</a>';

				};

				?>	

			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


				<!--right--><ul class="nav navbar-nav navbar-right">
				<ul class="nav navbar-nav">
	

					<?php if(isset ($_SESSION['status'])) {

						$status = $_SESSION['status'];

						if($status === "admin"){
							include_once("nav/admin.php");
						} else if($status === "guide"){
							include_once("nav/guide.php");
						} else if($status === "user"){
							include_once("nav/user.php");
						} 
					} else {
						include_once("nav/anon.php");
					} ?>	

				</ul>


			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>