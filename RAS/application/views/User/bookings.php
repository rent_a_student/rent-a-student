<div class="container">
	<div class="col-md-3"></div>
		<div class="col-md-6">
			<h1 class="text-center">Uw boekingen:</h1>
			<ul class="list-group">
				<?php foreach ($bookings as $b): ?>
					<li class="list-group-item">
						<h4><strong>Geboekt: </strong><?php echo $b['FirstNameGuide'] . " " . $b['LastNameGuide']; ?></h4>
						<p><strong>Datum: </strong><?php echo $b['DateBooking']; ?></p>
						<?php echo '<a class="btn btn-callblue" href="/rent-a-student/RAS/index.php/user/chat?userid=' . $_SESSION['id'] . '&guideid=' . $b['idGuide'] . '">Chat</a>'; ?>
					</li>		
				<?php endforeach ?>
			</ul>
		</div>
	<div class="col-md-3"></div>
</div>