<head>
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  	<link rel="stylesheet" href="/resources/demos/style.css">

</head>

<body>
	

<div class="container">
	<div class="col-md-1"></div>
	<div class="col-md-9">
		<h1 class="text-center">Boek nu je Gids!</h1>

		<?php foreach ($guideList as $guide ) {?>
		
		<ul>

			<li style= " height:250px; width:300px; margin-bottom:75px;float:left;">
				<div  class="panel panel-default panel-profile">
					<form action="" method="post">
						<h2 name="name"><?php echo $guide->FirstName." ".$guide->LastName ?></h2>
						<img style=" display="block" height=140px;" src="./../assets/profilepics/<?php echo $guide->Picture; ?>" alt="profilepic">
						<p class="user-info">
							<span><strong>Leeftijd: </strong><?php echo $guide->Age ?></span>
							<span><strong>IMD-jaar: </strong><?php echo $guide->IMDYear ?></span>
							<span><strong>Interesse: </strong> <?php echo $guide->Interest ?></span>
							<input type="hidden" name="idGuide" value="<?php echo $guide->id; ?>">
							<input type="hidden" name="firstname" value="<?php echo $guide->FirstName; ?>">
							<input type="hidden" name="lastname" value="<?php echo $guide->LastName; ?>">
							<input type="hidden" name="name" value="<?php echo $_SESSION['name']; ?>">
							<input type="hidden" name="id" value="<?php echo $_SESSION['id']; ?>">
							<!--<span><strong>Email: </strong> <?php //echo $guide->Email ?></span>	-->
						<p class="description"><strong>Bio: </strong><?php echo $guide->Bio ?></p>

						</p>

						<select name="date">
						<?php foreach ($date as $d ) {?>
							<option value="<?php echo $d['Date']; ?>"><?php echo $d['Date']; ?></option>
						<?php } ?>
						</select>
						<input class="btn btn-default" type="submit" value="boek nu" />
					</form>
				</div>

			</li>

		</ul>

		<?php } ?> 

	</div>
	<div class="col-md-1"></div>
</div>

</body>