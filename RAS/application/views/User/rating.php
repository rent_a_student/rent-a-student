<div class="container">
	<div class="col-md-3"></div>
		<div class="col-md-6">
			<div id="container">
				<?php echo validation_errors(); ?>

				<form class="form-horizontal" action="" method="post">

					<h2>Hoe vond u de gidsing?</h2>


					<select class="form-control" name="rating" id="rating">
						<option value="5">Uitstekend</option>
						<option value="4">Goed</option>
						<option value="3">Matig</option>
						<option value="2">Niet goed</option>
						<option value="1">slecht</option>
					</select>
					<br>


					<textarea class="form-control" name="quote" id="quote" placeholder="Beschrijf de dag in een korte quote." cols="30" rows="5"></textarea>
					<br>

					<textarea class="form-control" name="feedback" id="feedback" placeholder="Geef feedback over jouw gids." cols="30" rows="5"></textarea>
					</br>

					<button class="btn btn-default" type="submit" name="send">Send!</button>
				</form>
			</div>
		</div>
	<div class="col-md-3"></div>
</div>