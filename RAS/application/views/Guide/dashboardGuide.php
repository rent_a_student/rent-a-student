
<div class="container">
	<h1 class="text-center">Welkom op je eigen dashboard!</h1>

	<h2>Hieronder vind u een link om jezelf te laten raten, kopieer deze link en stuur deze naar de users!</h2>
	<input type="text" class="form-control" value="http://localhost/rent-a-student/RAS/index.php/user/rating?guideid=<?php echo $user[0]['id']; ?>">

	<h2 >Verander hieronder je persoonlijke gegevens.</h2>

	<form id="edit_form" action="" method="post" enctype="multipart/form-data">
		
		<div class="form-group">
			<label for="firstName">Voornaam</label>
			<input name="firstname" class="form-control" type="text" id="firstName" value="<?php echo $user[0]['FirstName']?>" placeholder="">
		</div>
		
		<div class="form-group">
			<label for="lastName">Achternaam</label>
			<input name="lastname" class="form-control" type="text" id="lastName" value="<?php echo $user[0]['LastName']?>" placeholder="">
		</div>

		<div class="form-group">
			<label for="email">Email</label>
			<input name="email" class="form-control" type="text" id="email" value="<?php echo $user[0]['Email']?>" placeholder="">
		</div>
		
		<div class="form-group">
			<label for="Age">leeftijd</label>
			<input name="age" class="form-control" type="text" id="Age" value="<?php echo $user[0]['Age']?>" placeholder="">
		</div>

		<div class="form-group">
			<label for="bio">Bio</label>
			<input name="bio"class="form-control" type="textfield" id="bio" value="<?php echo $user[0]['Bio']?>" placeholder="">
		</div>

		<div class="form-group">
			<label for="interest">Interest</label>
			<input name="interest"class="form-control" type="text" id="interest" value="<?php echo $user[0]['Interest']?>" placeholder="">
		</div>

		<!--INPUT-->
	     <?php //echo $user->profile_img_path; ?>
	     <input name = "profile_img_path" type="file" class="input-xlarge" id = "profile_img_path" />

		<div class="form-group">
		<button type="submit" class="btn btn-callred">Update!</button>
		</div>


	</form>
</div>