<div class="container">
	<div class="col-md-3"></div>
		<div class="col-md-6">
			<h1 class="text-center">Uw boekingen!</h1>
			<ul class="list-group">
				<?php foreach ($bookings as $b): ?>
					<li class="list-group-item">
						<form action="" method="post">
							<h4><strong>Geboekt door: </strong><?php echo $b['nameUser']; ?></h4>
							<p><strong>Datum: </strong><?php echo $b['DateBooking']; ?></p>
							<input type="hidden" id="idbooking" name="idbooking" value="<?php echo $b['id']; ?>">
							<input type="submit" value="Einde boeking" class="btn btn-primary">
							<?php echo '<a class="btn btn-primary" href="/rent-a-student/RAS/index.php/user/chat?userid=' . $b['idUser'] . '&guideid=' . $guideid . '">Chat</a>'; ?>
						</form>
					</li>		
				<?php endforeach ?>
			</ul>
		</div>
	<div class="col-md-3"></div>
</div>