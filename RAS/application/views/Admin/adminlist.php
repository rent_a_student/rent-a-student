<div class="container">
<div class="col-md-3"></div>
<div class="col-md-6">
<h1>Lijst van alle administrators</h1>
<ul class="list-group">
	<?php foreach ($admins as $a): ?>
		<li class="list-group-item">
			<form action="" method="post">
				<h2><?php echo $a['Username']; ?></h2>
				<p><?php echo $a['Email']; ?></p>
				<input type="hidden"  name="id" id="id" value="<?php echo $a['id']; ?>">
				<input type="submit" class="btn btn-default" value="verwijder">
			</form>
		</li>		
	<?php endforeach ?>
</ul>

<a href="registreer" class="btn btn-callred">Maak admins aan!</a>
</div>
<div class="col-md-3"></div>
</div>