<div class="container bookings">


<div class="col-md-6">
<h1>Alle boekingen:</h1>
<ul class="list-group">
	<?php foreach ($bookings as $b): ?>
		<li style="margin-top:5px;"class="list-group-item">
			<p class="user-info">
				<span><strong>Naam gids: </strong><?php echo $b['FirstNameGuide'] . " " . $b['LastNameGuide']; ?></span>
				<span><strong>Naam gebruiker: </strong><?php echo $b['nameUser']; ?></span>
				<span><strong>Datum: </strong><?php echo $b['DateBooking']; ?></span>

			</p>
		</li>		
	<?php endforeach ?>
</ul>

</div>
<div class="col-md-6">

<h1>Voeg datums toe:</h1>
<form action="" method="post">
      <p  style="fontstyle:bold; " >
      Datum: 
      <input type="date" name="date" placeholder="Kies hier je datum">
      </p>
		<input class="btn  btn-default btn-lg btn-block" type="submit" value="Voeg een datum toe" />


     </form>
</div>

</div>