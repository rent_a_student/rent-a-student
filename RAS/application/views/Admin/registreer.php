<div class="container">
<div class="col-md-3"></div>
<div class="col-md-6">

<?php echo validation_errors(); ?>
<h1>Maak een nieuwe admin aan</h1>

	<form action="" method="post">

		<div class="form-group">
		<label for="username">Username</label>
		<input type="text" id="username" name="username" class="form-control">
		</div>

		<div class="form-group">
		<label for="password">Wachtwoord</label>
		<input type="password" id="password" name="password" class="form-control">
		</div>
		
		<div class="form-group">
		<label for="email">Email</label>
		<input type="email" id="email" name="email" class="form-control">
		</div>
		
		<button class="btn btn-callred">Creëer admin!</button>
	</form>

	
	</div>
	<div class="col-md-3"></div>
</div>