<?php

class User extends CI_controller{

	function __Construct(){

		parent::__Construct();

		$this->load->database();
		$this->load->model("User_model");
		$this->load->model("Guide_model");
		$this->load->model("Date_model");
		$this->load->model("Quote_model");

	}


	public function guideList(){
	session_start();
	$data['guideList'] = $this->Guide_model->getAllGuides();
	$data['date'] = $this->Date_model->getAllDates();


	$b = new Guide_model();
	if(!empty($_POST)){
			$b->FirstNameGuide = $_POST["firstname"];
			$b->LastNameGuide = $_POST["lastname"];
			$b->idGuide = $_POST["idGuide"];
			$b->nameUser = $_POST["name"];
			$b->Bio = $_POST["bio"];
			$b->idUser = $_SESSION['id'];

			$b->idUser = $_POST["id"];
			$b->DateBooking = $_POST["date"];

			$this->Guide_model->booking($b);
			header("Location: /rent-a-student/RAS/index.php/user/boekingen");
	}
	
	$this->load->view('templates/header');
    $this->load->view('templates/nav');
	$this->load->view("user/guidelist", $data);
    $this->load->view('templates/footer');

	}

	public function dashboard(){

	
	
	$this->load->view('templates/header');
    $this->load->view('templates/nav');
	$this->load->view("user/dashboard");
    $this->load->view('templates/footer');


	}

	public function boekingen(){
		session_start();

		$id = $_SESSION['id'];
		$bookings = $this->User_model->getbookings($id);
		$viewdata = [
	    "bookings" => $bookings,
	    "userid" => $id
	    ];

		$this->load->view('templates/header');
	    $this->load->view('templates/nav');
		$this->load->view("user/bookings", $viewdata);
	    $this->load->view('templates/footer');
	}

	function chat(){
		session_start();
			$userid = $_GET['userid'];
			$guideid = $_GET['guideid'];
			// alle messages
			$allmessages = $this->User_model->getchat($userid, $guideid);

			$viewdata = [
				'allmessages' => $allmessages
			];

			//messages aanmaken
			$u = new User_model();

			if(!empty($_POST)){

				$message = $_POST['message'];

				if(isset($_SESSION['name'])){
					$name = $_SESSION['name'];
				}else{
					$id = $_SESSION['id'][0]['id'];
					$guide = $this->Guide_model->getInfo($id);
					$name = $guide[0]['FirstName'] . " " . $guide[0]['LastName'];
				}

				$u->name = $name;
				$u->userid = $userid;
				$u->guideid = $guideid;
				$u->message = $message;

				$this->User_model->sendmessage($u);

			}

			$this->load->view('templates/header');
		    $this->load->view('templates/nav');
			$this->load->view("chat/chat", $viewdata);
		    $this->load->view('templates/footer');
	}




	 public function rating() {
	 		$this->load->library('form_validation');
	 		$getid = $_GET['guideid'];
	 		if ($this->input->server('REQUEST_METHOD') == 'POST' && isset($_POST['send'])) {

	 			$quote = $this->input->post('quote');

	 			$feedback = $this->input->post('feedback');

	 			$rating = $this->input->post('rating');

	 			$this->form_validation->set_rules('quote', 'Quote', 'required');
	 			$this->form_validation->set_rules('feedback', 'Feedback', 'required');

	 			if ($this->form_validation->run() != FALSE) {
	         		$q = new Quote_model();
	         		$q->quote = $quote;
         			$q->feedback = $feedback;
	        		$q->guideID = $getid;
	        		$q->rating = $rating;
	        		$this->Quote_model->save($q);
        		}

			}
			$this->load->view('templates/header');
		    $this->load->view('templates/nav');
		    $this->load->view('templates/footer');
			$this->load->view('user/rating');
		}

}