<?php
class Home extends CI_Controller {

	public function view($page = 'home')
	{
		if ( ! file_exists(APPPATH.'/views/home/'.$page.'.php'))
		{
                // Whoops, we don't have a page for that!
			show_404();
		}

        $this->load->view('templates/header');
        $this->load->view('templates/nav');
        $this->load->view('home/'.$page);
        $this->load->view('templates/footer');
    }

    public function __construct()
    {
    	parent::__construct();
    	$this->load->model('Guide_model');
        $this->load->model('Admin_model');
    	$this->load->library('form_validation');
        $this->load->helper(array('form','url'));
        

    }


    public function registreer()
    {
    	$this->form_validation->set_rules('firstname', 'First Name', 'required|alpha', array('required' => 'Gelieve je voornaam in te vullen.' ,'alpha' => 'Je voornaam mag geen cijfers of leestekens bevatten.'));
    	$this->form_validation->set_rules('lastname', 'Last Name', 'required|alpha', array('required' => 'Gelieve je achternaam in te vullen.' ,'alpha' => 'Je achternaam mag geen cijfers of leestekens bevatten.'));
    	$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[12]', array('required' => 'Gelieve je een wachtwoord te kiezen.','min_length' => 'Je wachtwoord moet tussen de 5 en de 12 karakters lang zijn.','max_length' => 'Je wachtwoord moet tussen de 5 en de 12 karakters lang zijn.'));
    	$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[guide.Email]', array('required' => 'Gelieve je email adres in te vullen.','valid_email' => 'Geef een geldig email adres op.','is_unique' => 'Dit emailadres is al in gebruik.'));


        

    	if ($this->form_validation->run() == FALSE)
    	{
    		$this->load->view('templates/header');
            $this->load->view('templates/nav');
    		$this->load->view('home/registreer');
    		$this->load->view('templates/footer');
    	}
    	else
    	{
    		$g = new Guide_model();

    		$g->FirstName = $this->input->post("firstname");
    		$g->LastName = $this->input->post("lastname");
    		$g->Email = $this->input->post("email");
    		$g->password = $this->bcrypt->hash_password($this->input->post("password"));
    		$g->IMDYear = $this->input->post("IMDYear");
            $g->Picture = "default.png";
            $g->Bio = "Onbekend";
            $g->Interest = "Onbekend";
            $g->Age = "Onbekend";

           
             header("Location: /rent-a-student/RAS/index.php/guidelogin");
            //save() via model naar db
    		$this->Guide_model->save($g);

    		$this->load->view('templates/header');
            $this->load->view('templates/nav');
    		
            $this->load->view('home/login');
    		$this->load->view('templates/footer');


    	}
    
    }

    public function login()
    {
        session_start();

    	if (isset($_POST['email']) && isset($_POST['password'])) {
    		$login = $_POST['email'];
    		$password = $_POST['password'];

    		$user = $this->Guide_model-> getGuideByLogin($login, $password);
    		$loggedIn = ($user == null ? false : true);

            $id = $this->Guide_model->getid($login);

            if($loggedIn == true){
                header("Location: /rent-a-student/RAS/index.php/guide/boekingen");
                $_SESSION['id'] = $id;
                $_SESSION['email'] = $login;
                $_SESSION['status'] = "guide";
            }
    	}
    	$this->load->view('templates/header');
        $this->load->view('templates/nav');
    	$this->load->view('home/login');
    	$this->load->view('templates/footer');
    }

    public function admin()
    {
        session_start();

        if (isset($_POST['username']) && isset($_POST['password'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];

            $id = $this->Admin_model->idadmin($username);

            $user = $this->Admin_model->getAdminByLogin($username, $password);
            $loggedIn = ($user == null ? false : true);


            if($loggedIn == true){
                $_SESSION['id'] = $id;
                $_SESSION['name'] = $username;
                $_SESSION['status'] = "admin";
                header("Location: /rent-a-student/RAS/index.php/admin/dashboard");
            }
        }
        $this->load->view('templates/header');
        $this->load->view('templates/nav');
        $this->load->view('home/admin');
        $this->load->view('templates/footer');
    
        
    }
   

}