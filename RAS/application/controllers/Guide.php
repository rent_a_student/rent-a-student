<?php 
class Guide extends CI_Controller{

	function __Construct(){

		parent::__Construct();

		$this->load->database();
		$this->load->model("Guide_model");

	}

	public function boekingen(){
	session_start();
	$id = $_SESSION['id'][0]['id'];

	$bookings = $this->Guide_model->getbooking($id);
    
    $viewdata = [
    "bookings" => $bookings,
    "guideid" => $id
    ];

    if(!empty($_POST)){
    	$idbooking = $_POST['idbooking'];
    	$this->Guide_model->endbooking($idbooking);
    	header("Refresh:0");
    }
	
	$this->load->view('templates/header');
    $this->load->view('templates/nav');
    $this->load->view('Guide/bookings', $viewdata);
    $this->load->view('templates/footer');


	}

	 

    public function updateUser()
    {	
    	session_start();

	  	$id = $_SESSION['id'][0]['id'];
	

	  	$user = $this->Guide_model->getInfo($id);

	  	$data = [

	  		"user" => $user
	  	];




    	if(!empty($_POST)){

            $config['upload_path'] = './assets/profilepics';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 250;
            $this->load->library('upload', $config);
            $this->upload->do_upload('profile_img_path');
            $data_upload_files = $this->upload->data();
          
            $image = $data_upload_files['file_name']; 

            $this->upload->initialize($config);
            if (!$this->upload->do_upload('profile_img_path')) {
                $error = array('error' => $this->upload->display_errors());
                echo $error['error'];
            }
    	
    	
        $u = new Guide_model();

    		$u->FirstName = $this->input->post("firstname");
    		$u->LastName = $this->input->post("lastname");
    		$u->Email = $this->input->post("email");
    		$u->Age = $this->input->post("age");
    		$u->Bio = $this->input->post("bio");
    		$u->Interest = $this->input->post("interest");
    		$u->Password = $user[0]['Password'];
            $u->Picture = $image;
    		$u->IMDYear = $user[0]['IMDYear'];

    		header("Refresh:0");
    		
    		$this->Guide_model->updateUser($u,$id);

    		


    	}


 					
    		$this->load->view('templates/header');
            $this->load->view('templates/nav');
    		$this->load->view('guide/dashboardGuide',$data);
    		$this->load->view('templates/footer');


    }



	// public function email(){
// 			if($this->input->server('REQUEST_METHOD') == 'POST'){

// 				$this->load->library('email');

// 				$this->email->from('fqsdfqsdfqs.com', 'Jonas Reymen');
// 				$this->email->to('jonasr001@hotmail.com');

// 				$this->email->subject('Email Test');
// 				$this->email->message('test');	

// 				$this->email->send();

// 				echo $this->email->print_debugger();
// 			}

// 			$this->load->view("email/email.php");
// 		}
}
?>


