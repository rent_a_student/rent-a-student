<?php 
class Admin extends CI_Controller{

    public function __construct()
    {
    	parent::__construct();
    	$this->load->model('Admin_model');
      $this->load->model('User_model');
      $this->load->model('Date_model');
    	$this->load->library('form_validation');
    }

    public function dashboard()
    {
        session_start();
        $id = $_SESSION['id'][0]['id'];

        $admin = $this->Admin_model->getadminbyid($id);

        $viewdata = [
          "admin" => $admin
        ];


        $this->load->view('templates/header');
        $this->load->view('templates/nav');
        $this->load->view('admin/dashboard', $viewdata);
        $this->load->view('templates/footer');
    }


    public function registreer(){

        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[admin.Username]', array('required' => 'Gelieve een username in te vullen.','is_unique' => 'Deze username is al in gebruik.'));
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[12]', array('required' => 'Gelieve een wachtwoord te kiezen.','min_length' => 'Je wachtwoord moet tussen de 5 en de 12 karakters lang zijn.','max_length' => 'Je wachtwoord moet tussen de 5 en de 12 karakters lang zijn.'));
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[admin.Email]', array('required' => 'Gelieve je email adres in te vullen.','valid_email' => 'Geef een geldig email adres op.','is_unique' => 'Dit emailadres is al in gebruik.'));

			//na klik nieuwe admin aanmaken
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('templates/header');
            $this->load->view('templates/nav');
            $this->load->view('admin/registreer');
            $this->load->view('templates/footer');
        }
        else
        {
          $a = new Admin_model();

          $a->password = $this->bcrypt->hash_password($this->input->post("password"));
          $a->Email = $this->input->post("email");
          $a->Username = $this->input->post("username");

          $this->Admin_model->save($a);

        header("Location: /rent-a-student/RAS/index.php/admin/getalladmins");
      }

  }

  public function userlist(){
     $num_rows = $this->db->count_all_results('users');

     if($num_rows == 0){
        $status = "no user";
    } else {
        $status = "user";
    }

    $user = $this->User_model->getAll();

    $viewdata = [
    "status" => $status,
    "users" => $user
    ];
    
        $this->load->view('templates/header', $viewdata);
        $this->load->view('templates/nav', $viewdata);
        $this->load->view('admin/userlist', $viewdata);
        $this->load->view('templates/footer', $viewdata);
  }

  public function getalladmins(){
      $admins = $this->Admin_model->getadmins();
      $viewdata = [
        "admins" => $admins
      ];

      if(!empty($_POST)){
        $id = $_POST['id'];
        $this->Admin_model->verwijderadmin($id);

         header("Refresh:0");
      }

     

      $this->load->view('templates/header');
      $this->load->view('templates/nav');
      $this->load->view('admin/adminlist', $viewdata);
      $this->load->view('templates/footer');
  }

  public function bookings(){

    $bookings = $this->Admin_model->allbookings();

    $viewdata = [
        "bookings" => $bookings
    ];

    $d = new Date_model();

    if(!empty($_POST)){
      $d->Date = $_POST['date'];
      //header("Refresh:0");
      $this->Date_model->saveDate($d);
    }

  
    $this->load->view('templates/header');
    $this->load->view('templates/nav');
    $this->load->view('admin/bookings', $viewdata);
    $this->load->view('templates/footer');
  }
}

