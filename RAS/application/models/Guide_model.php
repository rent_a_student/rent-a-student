<?php 
class Guide_model extends CI_Model{
	public $FirstName;
	public $LastName;
	public $Age = "";
	public $password;
	public $Email;
	public $IMDYear;
	public $Picture ="";
	public $Bio  = "";
	public $Interest  = "";

	private $TotalBookings;
	private $TotalRating;

	function __construct()
	{
	    $this->load->database();
		parent::__construct();
	}

	public function save($g){
		$this->db->insert('guide', $g);
	}

	function getGuideByLogin($email, $password) {        
		$this->db->where('Email',$email);

		$result = $this->getGuide($password);

		if (!empty($result)) {
			return $result;
		} else {
			$feedback = "Wrong password or email";
			return null;
		}
	}
	
	function getGuide($password) {
		$query = $this->db->get('guide');

		if ($query->num_rows() > 0) {

			$result = $query->row_array();

			if ($this->bcrypt->check_password($password, $result['Password'])) {
				echo "Welcome!";
				return $result;
			} 
		}
	}

	function getAllGuides(){

		$this->db->select("id,FirstName,LastName,Email,Age,Interest,IMDYear,Picture,Bio");
		$this->db->from("guide");
		$query = $this->db->get();

		return $query->result();
		
	}

	function booking($t){
		
		$data = [
			"idGuide" => $t->idGuide,
			"FirstNameGuide" => $t->FirstNameGuide,
			"LastNameGuide" => $t->LastNameGuide,
			"nameUser" => $t->nameUser,
			"idUser" => $t->idUser,
			"DateBooking" => $t->DateBooking
		];
		$this->db->insert('bookings', $data);

		$this->db->from('guide');
		$this->db->where('id', $t->idGuide);
		$this->db->update('guide', ['TotalBookings' => 'TotalBookings' + 1]); 
	}

	function getid($email){
		$this->db->select("id");
		$this->db->from("guide");
		$this->db->where('Email',$email);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result_array();
	}

	function getbooking($id){
		$this->db->from("bookings");
		$this->db->where('idGuide',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

	function getInfo($id){

		$this->db->from("guide");
		$this->db->where("id",$id);
		$query = $this->db->get();
		return $query->result_array();

	}

	function updateUser($u,$id){

		$this->db->select("*");
		$this->db->from("guide");
		$this->db->where('id',$id);
		$this->db->limit(1);
		$query = $this->db->update('guide',$u);

	}

	function endbooking($idbooking){
		$this->db->from("bookings");
		$this->db->where('id',$idbooking);
		$this->db->delete();
	}

	
}
