<?php 
	 class Quote_model extends CI_Model {

	 	public $quote;
	 	public $feedback;

	 	public function get_all(){
	 		$this->db->order_by('id', 'RANDOM');
 	    		$this->db->limit(1);
	 		$quotes = $this->db->get("quote");
	 		return $quotes->result_array();
	 	}

	 	public function save($q) {
	 		$this->db->insert("quote", $q);
	 	}
	 }