<?php 
class Admin_model extends CI_Model{
	public $password = "";
	public $Email = "";
	public $Username = "";
	public $picture = "";


	function __construct()
	{
		$this->load->database();
		parent::__construct();
	}

	public function save($t){
		$this->db->insert('admin', $t);
	}

	function getAdminByLogin($username, $password) {        
		$this->db->where('Username',$username);

		$result = $this->getAdmin($password);

		if (!empty($result)) {
			return $result;
		} else {
			$feedback = "Wrong password or email";
			return null;
		}
	}
	
	function getAdmin($password) {
		$query = $this->db->get('admin');

		if ($query->num_rows() > 0) {

			$result = $query->row_array();

			if ($this->bcrypt->check_password($password, $result['Password'])) {
				echo "Welcome!";
				return $result;
			} 
		}
	}

	function getadmins(){
		$quotes = $this->db->get("admin");
		return $quotes->result_array();
	}

	function allbookings(){
		$bookings = $this->db->get("bookings");
		return $bookings->result_array();
	}

	function idadmin($username){
		$this->db->select("id");
		$this->db->from("admin");
		$this->db->where('Username',$username);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result_array();
	}

	function getadminbyid($id){
		$this->db->from("admin");
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

	function changesettings($a,$id){
		$this->db->from("admin");
		$this->db->where('id', $id);
		$this->db->limit(1);
		$this->db->update('admin', $a);
	}

	function verwijderadmin($idadmin){
		$this->db->from("admin");
		$this->db->where('id', $idadmin);
		$this->db->delete();
	}

}