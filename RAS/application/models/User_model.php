<?php 
    class User_model extends CI_Model
    {

        function __construct()
        {
            parent::__construct();
        }

        // function get_user($username, $password)
        // {

        //     $this->db->select("username","password");
        //                 $this->db->from("admin");
        //                 $this->db->where('username',$username);
        //                 $this->db->where('password',$password);
        //                 $this->db->limit(1);

        //                 $query = $this->db->get();
        //             return $query->num_rows();
        // }

        function getAll(){
            $all = $this->db->get("users");
            return $all->result_array();
        }

        function getbookings($id){
            $this->db->from("bookings");
            $this->db->where('idUser',$id);
            $query = $this->db->get();
            return $query->result_array();
        }

        function getchat($userid, $guideid){
            $this->db->from("chat");
            $this->db->where('userid', $userid);
            $this->db->where('guideid', $guideid);
            $query = $this->db->get();
            return $query->result_array();
        }

        function sendmessage($u){
            $this->db->insert('chat', $u);
        }
    

    }