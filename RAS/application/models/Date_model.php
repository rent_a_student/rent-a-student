<?php 
class Date_model extends CI_Model{


	function __construct()
	{
		$this->load->database();
		parent::__construct();
	}

	public function saveDate($d){
		$this->db->insert('datums', $d);
	}

	public function getAllDates(){
		$dates = $this->db->get("datums");
		return $dates->result_array();
	}

}